package casbin

import "github.com/nilorg/naas/internal/module/casbin"

var (
	// MyDomKeyMatch2Func 定义域KeyMatch2
	MyDomKeyMatch2Func = casbin.MyDomKeyMatch2Func
	// MyRegexMatchFunc 定义域RegexMatch
	MyRegexMatchFunc = casbin.MyRegexMatchFunc
)
